# Prérequis
 - PHP 7.4
 - Symfony 5
 - Git
 - Composer
 
# Installation

1. Clonez le dépot
    `git clone https://gitlab.com/abderrahmen2387/jeu-de-carte`
2. Installez les dépendances avec composer
    `composer install`
3. Lancez le server
    `symfony serve` si symfony binary est installé
    ou `php -S localhost:3000 -t public`

