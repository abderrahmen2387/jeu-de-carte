<?php

namespace App\Controller;

use App\Service\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller of the game
 *
 * @package App\Controller
 */
class GameController extends AbstractController
{
    /**
     * @param GameService $gameService  manager game
     * @return Response
     *
     * @Route("/", name="game")
     */
    public function index(GameService $gameService): Response
    {
        $notSortedCards = $gameService->getCards();

        return $this->render('game/index.html.twig', [
            'colors' => $gameService->getColors(),
            'values' => $gameService->getValuesCard(),
            'notSortedCards' => $notSortedCards,
            'sortedCards' => $gameService->sortCards($notSortedCards)
        ]);
    }
}
