<?php

namespace App\Model;

/**
 * Model of the card
 *
 * @package App\Model
 */
class Card
{
    /**
     * Color of the card
     *
     * @var string  $color
     */
    private string $color;

    /**
     * Value of the card
     *
     * @var string  $value
     */
    private string $value;

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return $this
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->value.$this->color;
    }
}
