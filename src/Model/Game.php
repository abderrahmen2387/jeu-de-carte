<?php

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Model of the game
 *
 * @package App\Model
 */
class Game
{
    /**
     * Nb cards in a hand
     */
    const NB_CARDS_IN_HAND = 10;

    /**
     * all colors in the game
     */
    const COLORS = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];

    /**
     * all values in the game
     */
    const VALUES_CARDS = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];

    /**
     * List of cards in the game
     *
     * @var ArrayCollection $cards
     */
    private ArrayCollection $cards;

    /**
     * Create the game
     */
    public function __construct()
    {
        $this->cards = new ArrayCollection();
        foreach (self::VALUES_CARDS as $value) {
            foreach (self::COLORS as $color) {
                $card = (new Card())->setValue($value)->setColor($color);
                $this->addCard($card);
            }
        }
    }

    /**
     * @return Collection|Card[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    /**
     * @param Card $card
     *
     * @return $this
     */
    public function addCard(Card $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
        }

        return $this;
    }
}
