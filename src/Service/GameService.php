<?php

namespace App\Service;

use App\Model\Game;

/**
 * Class GameService
 *
 * @package App\Service
 */
class GameService
{
    /**
     * List of colors
     *
     * @var string[]
     */
    private array $colors;

    /**
     * List of values
     *
     * @var string[]
     */
    private array $valuesCards;

    /**
     * Initialise the attributes of the service game
     * and generate a random color and values card
     */
    public function __construct()
    {
        $this->colors = Game::COLORS;
        $this->valuesCards = Game::VALUES_CARDS;
        shuffle($this->colors);
        shuffle($this->valuesCards);
    }

    /**
     * Return list of color in random order
     *
     * @return string[]
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * Return list of card values in random order
     *
     * @return string[]
     */
    public function getValuesCard()
    {
        return $this->valuesCards;
    }

    /**
     * Construct a hand with 10 card and sort it
     *
     * @return array
     */
    public function getCards()
    {
        $cardsGame = (new Game())->getCards()->toArray();
        $cardsNotSorted = [];

        foreach (array_rand($cardsGame, Game::NB_CARDS_IN_HAND) as $key) {
            $cardsNotSorted[] = $cardsGame[$key];
        }

        return $cardsNotSorted;
    }

    /**
     * sort a hand of 10 cards
     *
     * @param array $cards  list of cards to be sorted
     *
     * @return array
     */
    public function sortCards(array $cards)
    {
        usort($cards, function($a, $b) {
            if ($a->getColor() == $b->getColor()) {
                return (array_search($a->getValue(), $this->valuesCards) < array_search($b->getValue(), $this->valuesCards)) ? -1 : 1;
            } else {
                return (array_search($a->getColor(), $this->colors) < array_search($b->getColor(), $this->colors)) ? -1 : 1;
            }
        });

        return $cards;
    }
}
